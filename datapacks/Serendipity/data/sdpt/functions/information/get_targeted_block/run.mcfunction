function sdpt:information/get_direction_vector/run

# 1. 获得初始的精确坐标
execute store result score #a sdpt.information.get_targeted_block.number run data get entity @s Pos[0] 1000
execute store result score #b sdpt.information.get_targeted_block.number run data get entity @s Pos[1] 1000
scoreboard players add #b sdpt.information.get_targeted_block.number 1000
execute store result score #c sdpt.information.get_targeted_block.number run data get entity @s Pos[2] 1000

# 2. 获得向下取整后的坐标
execute store result score #pos_xi sdpt.information.get_targeted_block.number run data get entity @s Pos[0]
execute store result score #pos_yi sdpt.information.get_targeted_block.number run data get entity @s Pos[1]
execute store result score #pos_zi sdpt.information.get_targeted_block.number run data get entity @s Pos[2]
scoreboard players add #pos_yi sdpt.information.get_targeted_block.number 1
scoreboard players operation #pos_xi sdpt.information.get_targeted_block.number *= #1000 sdpt.information.get_targeted_block.number
scoreboard players operation #pos_yi sdpt.information.get_targeted_block.number *= #1000 sdpt.information.get_targeted_block.number
scoreboard players operation #pos_zi sdpt.information.get_targeted_block.number *= #1000 sdpt.information.get_targeted_block.number

# 生成检测实体
summon marker ~ ~ ~ {Tags:[sdpt.inforamtion.get_targeted_block.checker]}

# 3. 设置层数，计算y，并且向下取整
scoreboard players set #current sdpt.information.get_targeted_block.number 1000
function sdpt:information/get_targeted_block/body/y

# 4. 依次遍历 [y0, y] 间的所有坐标
scoreboard players operation #while sdpt.information.get_targeted_block.number = #pos_yi sdpt.information.get_targeted_block.number
function sdpt:information/get_targeted_block/body/while

# 3. 设置层数，计算y，并且向下取整
scoreboard players set #current sdpt.information.get_targeted_block.number 2000
function sdpt:information/get_targeted_block/body/y

# 4. 依次遍历 [y0, y] 间的所有坐标
scoreboard players operation #while sdpt.information.get_targeted_block.number = #pos_yi sdpt.information.get_targeted_block.number
function sdpt:information/get_targeted_block/body/while