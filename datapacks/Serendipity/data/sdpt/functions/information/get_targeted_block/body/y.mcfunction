# 计算 y

scoreboard players operation #n/m sdpt.information.get_targeted_block.number = @s sdpt.information.get_direction_vector.motion_y
scoreboard players operation #n/m sdpt.information.get_targeted_block.number /= @s sdpt.information.get_direction_vector.motion_x

scoreboard players operation #x-a sdpt.information.get_targeted_block.number = #pos_xi sdpt.information.get_targeted_block.number
scoreboard players operation #x-a sdpt.information.get_targeted_block.number += #current sdpt.information.get_targeted_block.number
scoreboard players operation #x-a sdpt.information.get_targeted_block.number -= #a sdpt.information.get_targeted_block.number

scoreboard players operation #n/m*(x-a) sdpt.information.get_targeted_block.number = #n/m sdpt.information.get_targeted_block.number
scoreboard players operation #n/m*(x-a) sdpt.information.get_targeted_block.number *= #x-a sdpt.information.get_targeted_block.number

scoreboard players operation #y sdpt.information.get_targeted_block.number = #b sdpt.information.get_targeted_block.number
scoreboard players operation #y sdpt.information.get_targeted_block.number += #n/m*(x-a) sdpt.information.get_targeted_block.number

# 向下取整
scoreboard players operation #y sdpt.information.get_targeted_block.number /= #1000 sdpt.information.get_targeted_block.number
scoreboard players operation #y sdpt.information.get_targeted_block.number *= #1000 sdpt.information.get_targeted_block.number


say y