execute if score #while sdpt.information.get_targeted_block.number > #y sdpt.information.get_targeted_block.number run return fail

execute store result entity @e[limit=1, tag=sdpt.inforamtion.get_targeted_block.checker] Pos[0] double 0.001 run scoreboard players get #pos_xi sdpt.information.get_targeted_block.number
execute store result entity @e[limit=1, tag=sdpt.inforamtion.get_targeted_block.checker] Pos[1] double 0.001 run scoreboard players get #while sdpt.information.get_targeted_block.number
execute store result entity @e[limit=1, tag=sdpt.inforamtion.get_targeted_block.checker] Pos[2] double 0.001 run scoreboard players get #pos_zi sdpt.information.get_targeted_block.number

execute as @e[limit=1, tag=sdpt.inforamtion.get_targeted_block.checker] at @s unless block ~ ~ ~ air run return 1

say while

scoreboard players add #while sdpt.information.get_targeted_block.number 1000
function sdpt:information/get_targeted_block/body/while