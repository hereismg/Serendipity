function sdpt:information/get_direction_vector/run

# 1. 获得初始的精确坐标 
## 注意，后面要把初始精确坐标移动到玩家的眼睛处！
execute store result score #a sdpt.information.get_targeted_block.number run data get entity @s Pos[0] 1000
execute store result score #b sdpt.information.get_targeted_block.number run data get entity @s Pos[1] 1000
scoreboard players add #b sdpt.information.get_targeted_block.number 1500
execute store result score #c sdpt.information.get_targeted_block.number run data get entity @s Pos[2] 1000

# 2. 获得向下取整后的坐标
execute store result score #x_0 sdpt.information.get_targeted_block.number run data get entity @s Pos[0]
execute store result score #y_0 sdpt.information.get_targeted_block.number run data get entity @s Pos[1]
execute store result score #z_0 sdpt.information.get_targeted_block.number run data get entity @s Pos[2]
scoreboard players add #y_0 sdpt.information.get_targeted_block.number 1
scoreboard players operation #x_0 sdpt.information.get_targeted_block.number *= #1000 sdpt.information.get_targeted_block.number
scoreboard players operation #y_0 sdpt.information.get_targeted_block.number *= #1000 sdpt.information.get_targeted_block.number
scoreboard players operation #z_0 sdpt.information.get_targeted_block.number *= #1000 sdpt.information.get_targeted_block.number

# 生成检测实体
summon marker ~ ~ ~ {Tags:[sdpt.inforamtion.get_targeted_block.checker]}

## 第一层
scoreboard players operation #x_i sdpt.information.get_targeted_block.number = #x_0 sdpt.information.get_targeted_block.number

# 计算下方块坐标
scoreboard players operation #y_down sdpt.information.get_targeted_block.number = #y_0 sdpt.information.get_targeted_block.number

# 计算上方块坐标
scoreboard players add #x_i sdpt.information.get_targeted_block.number 1000
function sdpt:information/get_targeted_block.fun/body/x_to_y
scoreboard players operation #y_up sdpt.information.get_targeted_block.number = #y_i sdpt.information.get_targeted_block.number
scoreboard players remove #x_i sdpt.information.get_targeted_block.number 1000

# 循环，求出，从下方块到上方块之间的方块
execute if function sdpt:information/get_targeted_block.fun/body/while if function sdpt:information/get_targeted_block.fun/body/success run return 1

## 第二层
# say 第二层
scoreboard players add #x_i sdpt.information.get_targeted_block.number 1000

# 计算上方块坐标
function sdpt:information/get_targeted_block.fun/body/x_to_y
scoreboard players operation #y_down sdpt.information.get_targeted_block.number = #y_i sdpt.information.get_targeted_block.number

# 计算下方块坐标
scoreboard players add #x_i sdpt.information.get_targeted_block.number 1000
function sdpt:information/get_targeted_block.fun/body/x_to_y
scoreboard players operation #y_up sdpt.information.get_targeted_block.number = #y_i sdpt.information.get_targeted_block.number
scoreboard players remove #x_i sdpt.information.get_targeted_block.number 1000

# 循环，求出，从下方块到上方块之间的方块
execute if function sdpt:information/get_targeted_block.fun/body/while if function sdpt:information/get_targeted_block.fun/body/success run return 1




scoreboard players add #x_i sdpt.information.get_targeted_block.number 1000

# 计算上方块坐标
function sdpt:information/get_targeted_block.fun/body/x_to_y
scoreboard players operation #y_down sdpt.information.get_targeted_block.number = #y_i sdpt.information.get_targeted_block.number

# 计算下方块坐标
scoreboard players add #x_i sdpt.information.get_targeted_block.number 1000
function sdpt:information/get_targeted_block.fun/body/x_to_y
scoreboard players operation #y_up sdpt.information.get_targeted_block.number = #y_i sdpt.information.get_targeted_block.number
scoreboard players remove #x_i sdpt.information.get_targeted_block.number 1000

# 循环，求出，从下方块到上方块之间的方块
execute if function sdpt:information/get_targeted_block.fun/body/while if function sdpt:information/get_targeted_block.fun/body/success run return 1
