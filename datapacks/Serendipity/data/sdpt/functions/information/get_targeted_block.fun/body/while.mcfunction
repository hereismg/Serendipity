execute if score #y_down sdpt.information.get_targeted_block.number > #y_up sdpt.information.get_targeted_block.number run return fail

execute store result entity @e[limit=1, tag=sdpt.inforamtion.get_targeted_block.checker] Pos[0] double 0.001 run scoreboard players get #x_i sdpt.information.get_targeted_block.number
execute store result entity @e[limit=1, tag=sdpt.inforamtion.get_targeted_block.checker] Pos[1] double 0.001 run scoreboard players get #y_down sdpt.information.get_targeted_block.number
execute store result entity @e[limit=1, tag=sdpt.inforamtion.get_targeted_block.checker] Pos[2] double 0.001 run scoreboard players get #z_0 sdpt.information.get_targeted_block.number

execute as @e[limit=1, tag=sdpt.inforamtion.get_targeted_block.checker] at @s unless block ~ ~ ~ air run return 1

say while

scoreboard players add #y_down sdpt.information.get_targeted_block.number 1000
function sdpt:information/get_targeted_block.fun/body/while