# ============================
# @brief    计算 y
# @param    需要指定 x_i
# ============================

# 计算 n/m
scoreboard players operation #n/m sdpt.information.get_targeted_block.number = @s sdpt.information.get_direction_vector.motion_y
scoreboard players operation #n/m sdpt.information.get_targeted_block.number /= @s sdpt.information.get_direction_vector.motion_x

# 计算 x-a
scoreboard players operation #x-a sdpt.information.get_targeted_block.number = #x_i sdpt.information.get_targeted_block.number
scoreboard players operation #x-a sdpt.information.get_targeted_block.number -= #a sdpt.information.get_targeted_block.number

# 计算 n/m * (x-a)
scoreboard players operation #n/m*(x-a) sdpt.information.get_targeted_block.number = #n/m sdpt.information.get_targeted_block.number
scoreboard players operation #n/m*(x-a) sdpt.information.get_targeted_block.number *= #x-a sdpt.information.get_targeted_block.number

# 得到 y
scoreboard players operation #y_i sdpt.information.get_targeted_block.number = #b sdpt.information.get_targeted_block.number
scoreboard players operation #y_i sdpt.information.get_targeted_block.number += #n/m*(x-a) sdpt.information.get_targeted_block.number

# 向下取整
scoreboard players operation #y_i sdpt.information.get_targeted_block.number /= #1000 sdpt.information.get_targeted_block.number
scoreboard players operation #y_i sdpt.information.get_targeted_block.number *= #1000 sdpt.information.get_targeted_block.number

say y