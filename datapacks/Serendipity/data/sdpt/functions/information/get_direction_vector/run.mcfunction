# ================================
# @brief        生成执行者的视角，最后生成的方向向量存储在计分板中，并且放大 1000 倍
# @position     entity
# @executor     entity
# @facing       entity
# ================================

# 1. 首先生成一个辅助实体
execute at @s run summon minecraft:marker ^ ^ ^-1 {Tags:["sdpt.information.get_direction_vector.temp"]}

# 2. 将该实体的坐标存储在对应的记分板中
execute as @e[limit=1,distance=..2,tag=sdpt.information.get_direction_vector.temp] at @s store result score @s sdpt.information.get_direction_vector.motion_x run data get entity @s Pos[0] 1000
execute as @e[limit=1,distance=..2,tag=sdpt.information.get_direction_vector.temp] at @s store result score @s sdpt.information.get_direction_vector.motion_y run data get entity @s Pos[1] 1000
execute as @e[limit=1,distance=..2,tag=sdpt.information.get_direction_vector.temp] at @s store result score @s sdpt.information.get_direction_vector.motion_z run data get entity @s Pos[2] 1000

# 3. 将执行体的坐标存储在对应的记分板中
execute store result score @s sdpt.information.get_direction_vector.motion_x run data get entity @s Pos[0] 1000
execute store result score @s sdpt.information.get_direction_vector.motion_y run data get entity @s Pos[1] 1000
execute store result score @s sdpt.information.get_direction_vector.motion_z run data get entity @s Pos[2] 1000

# 4. 做减法，得到方向向量
scoreboard players operation @s sdpt.information.get_direction_vector.motion_x -= @e[limit=1,distance=..2,tag=sdpt.information.get_direction_vector.temp] sdpt.information.get_direction_vector.motion_x 
scoreboard players operation @s sdpt.information.get_direction_vector.motion_y -= @e[limit=1,distance=..2,tag=sdpt.information.get_direction_vector.temp] sdpt.information.get_direction_vector.motion_y 
scoreboard players operation @s sdpt.information.get_direction_vector.motion_z -= @e[limit=1,distance=..2,tag=sdpt.information.get_direction_vector.temp] sdpt.information.get_direction_vector.motion_z

# 5. 卸磨杀驴
kill @e[tag=sdpt.information.get_direction_vector.temp]
