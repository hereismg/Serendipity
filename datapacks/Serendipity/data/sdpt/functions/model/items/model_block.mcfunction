execute unless entity @s[nbt={SelectedItem:{tag:{id:'item.sdpt.model_block'}}}] run return fail

function sdpt:information/get_selected_block/run

summon block_display ~ ~-2 ~ {Tags:["sdpt.model.build_model_block", "sdpt.model.build_model_block.temp"], block_state:{Name:"minecraft:scaffolding"}}
tag @s add sdpt.model.build_model_block.executor

execute as @e[tag=sdpt.model.build_model_block.temp] store result entity @s Pos[0] double 0.001 run scoreboard players get @e[tag=sdpt.model.build_model_block.executor,limit=1] sdpt-information-selected_block-x
execute as @e[tag=sdpt.model.build_model_block.temp] store result entity @s Pos[1] double 0.001 run scoreboard players get @e[tag=sdpt.model.build_model_block.executor,limit=1] sdpt-information-selected_block-y
execute as @e[tag=sdpt.model.build_model_block.temp] store result entity @s Pos[2] double 0.001 run scoreboard players get @e[tag=sdpt.model.build_model_block.executor,limit=1] sdpt-information-selected_block-z

execute as @e[tag=sdpt.model.build_model_block.temp] at @s align xyz run tp @s ~ ~ ~

execute as @e[tag=sdpt.model.build_model_block.temp] at @s run setblock ~ ~ ~ barrier replace

tag @e[tag=sdpt.model.build_model_block.temp] remove sdpt.model.build_model_block.temp
tag @p[tag=sdpt.model.build_model_block.executor] remove sdpt.model.build_model_block.executor
