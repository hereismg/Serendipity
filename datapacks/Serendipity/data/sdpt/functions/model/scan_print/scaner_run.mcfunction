# =====================
# @brief    循环体，从for.from循环到for.to，模拟for循环
# =====================

execute if score #for.from sdpt.model.number <= #for.to sdpt.model.number

scoreboard players set #for.from2 sdpt.model.number 1
execute store result score #for.to2 sdpt.model.number run data get entity @s data.scan_area.size[0]
function sdpt:model/scan_print/scaner_run2

# 将扫描执行体传送到下一行的起点
tp ~ ~ ~1
execute store result entity @s Pos[2] double 1.0 run data get entity @e[limit=1, tag=sdpt.model.scan_print.scaner] Pos[2]


scoreboard players add #for.from sdpt.model.number 1
function sdpt:model/scan_print/scaner_run