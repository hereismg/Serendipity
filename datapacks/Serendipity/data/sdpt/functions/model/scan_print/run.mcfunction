# =================================
# @brief        扫描一个平面的全部方块到指定位置
# @executor     block_display
# =================================

summon marker ~ ~ ~ {Tags:[sdpt.model.scan_print.scaner]}
data modify entity @e[tag=sdpt.model.scan_print.scaner,limit=1] data set from entity @s data
data modify entity @e[tag=sdpt.model.scan_print.scaner,limit=1] Pos set from entity @s data.scan_area.anchor

summon marker ~ ~ ~ {Tags:[sdpt.model.scan_print.scaner.run]}
data modify entity @e[tag=sdpt.model.scan_print.scaner.run,limit=1] data set from entity @s data
data modify entity @e[tag=sdpt.model.scan_print.scaner.run,limit=1] Pos set from entity @s data.scan_area.anchor

summon marker ~ ~ ~ {Tags:[sdpt.model.scan_print.printer]}
data modify entity @e[tag=sdpt.model.scan_print.printer,limit=1] data set from entity @s data
data modify entity @e[tag=sdpt.model.scan_print.printer,limit=1] data set from entity @s data.print_area.anchor

