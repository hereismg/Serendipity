function sdpt:information/get_current_block_state/run

scoreboard players operation #rate sdpt.model.number = @s data.print_area.size[0]

execute summon block_display run data modify entity @s block_state set from entity @e[tag=sdpt.model.scan_print.scaner,limit=1] current_block_state