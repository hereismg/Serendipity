# =====================
# @brief    循环体，从for.from循环到for.to，模拟for循环
# =====================

execute if score #for.from2 sdpt.model.number <= #for.to2 sdpt.model.number

# 获取当前的方块状态
function sdpt:information/get_current_block_state


# 向前传送一格
tp ~1 ~ ~

scoreboard players add #for.from2 sdpt.model.number 1
function sdpt:model/scan_print/scaner_run2